//
//  BottomSheetView+BottomSheet.swift
//  BottomSheet
//
//  Created by Daneo Van Overloop on 26/02/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import Foundation

/// Contract for the BottomSheet's delegate
public protocol BottomSheetDelegate: class {
    /// Typealias pointing towards the possible states of the sheet
    typealias State = SheetState

    /// Called after the `BottomSheet` changes state
    func changed(to: State)

    /// Called after the `BottomSheet` has been dismissed
    func dismissed()
}

/// Contract defining a BottomSheet
public protocol BottomSheet: class {
    typealias State = SheetState

    var state: State { get }

    /// The container for the `contentView`
    var containerView: UIView { get }

    /// The view that will be presented in the BottomSheet
    var contentView: UIView? { get set }

    /// Delegate that will be informed of state changes
    var delegate: BottomSheetDelegate? { get set }

    /// Used by the gesture to alter the top constraint and move the view
    var topOffset: CGFloat { get set }

    /// Images that have have to be used in either `expanded` or `collapsed` state
    var images: (scrollDownImage: UIImage?, scrollUpImage: UIImage?) { get set }

    /// Initializes with a chosen state, and configure the view accordingly
    /// - Parameter initialState: state for the view to start from
    init(initialState: State)

    /// Change the view's state
    /// - Parameter to: destination state to animate to
    /// - Parameter animated: indicating whether the state change is animated
    /// - Parameter completion: block executed after the state change has finished
    func change(to: State, animated: Bool, completion: @escaping () -> Void)

    /// Dismiss the view
    /// - Parameter animated: indicating whether dismissal is animated
    /// - Parameter completion: block executed after the dismissal has finished
    func dismiss(animated: Bool, completion: ((Bool) -> Void)?)

    /// Prepares the sheet for presentation
    func prepareForPresentation()
}

public extension BottomSheet {
    /**
     Changes to the supplied state
     - Parameter to: state to animate to
     - SeeAlso: `BottomSheet.change(to:completion:)`
     */
    func change(to state: State, animated: Bool = true) {
        change(to: state, animated: animated, completion: {})
    }

    /**
     Changes the view's state to .collapsed
     - Parameter animated: indicating whether the state change is animated
     - Parameter completion: block executed after the state change has finished
     - SeeAlso: `BottomSheet.change(to:completion:)`
     */
    func collapse(animated: Bool = true, completion: @escaping () -> Void = {}) {
        change(to: .collapsed, animated: animated, completion: completion)
    }

    /**
     Changes the view's state to .expanded
     - Parameter animated: indicating whether the state change is animated
     - Parameter completion: block executed after the state change has finished
     - SeeAlso: `BottomSheet.change(to:completion:)`
     */
    func expand(animated: Bool = true, completion: @escaping () -> Void = {}) {
        change(to: .expanded, animated: animated, completion: completion)
    }

    /**
     Dismiss the view
     - Parameter animated: indicating whether dismissal is animated
     - SeeAlso: `BottomSheet.dismiss(animated:completion:)`
     */
    func dismiss(animated: Bool = true) {
        dismiss(animated: animated, completion: nil)
    }
}

public extension BottomSheet {
    /// Accessibility identifier for the entire sheet. Mostly used in Automated Tests
    static var bottomSheetIdentifier: String {
        return "bottomSheet"
    }

    /// Accessibility identifier for the sheet's content. Mostly used in Automated Tests
    static var sheetContentIdentifier: String {
        return "bottomSheetContent"
    }

    /// Accessibility identifier for the sheet's dismiss button. Mostly used in Automated Tests
    static var dismissButtonIdentifier: String {
        return "dismissButton"
    }

    /// Accessibility identifier for the sheet's background view. Mostly used in Automated Tests
    static var backgroundViewIdentifier: String {
        return "background"
    }
}

public extension BottomSheet where Self: UIView {
    /// Configures the default Accessibility Identifiers on the Sheet and the content
    func configureIdentifiers() {
        accessibilityIdentifier = Self.bottomSheetIdentifier
        contentView?.accessibilityIdentifier = Self.sheetContentIdentifier
    }
}
