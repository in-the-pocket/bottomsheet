//
//  SheetScrollView.swift
//  BottomSheet
//
//  Created by Daneo Van Overloop on 06/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import Anchorage
import UIKit

class SheetScrollView: UIScrollView {
    var contentView: UIView? {
        willSet {
            contentView?.removeFromSuperview()
        }
        didSet {
            guard let view = contentView else {
                return
            }
            super.addSubview(view)
            view.edgeAnchors == edgeAnchors
            view.widthAnchor == widthAnchor
            view.heightAnchor == heightAnchor ~ .low
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    func configure() {
        showsHorizontalScrollIndicator = false
        delaysContentTouches = false
    }

    override func addSubview(_ view: UIView) {
        // Do nothing
        // Subviews should be added to the contentView
    }
}
