//
//  BottomSheetController.swift
//  BottomSheet
//
//  Created by Daneo Van Overloop on 13/02/2019.
//  Copyright © 2019 Swiftin. All rights reserved.
//

import Anchorage
import Foundation
import UIKit

/** The controller presenting the BottomSheet
 - Note: This component should be added as a childViewController and
 then be notified of this through `didMoveToParent`
 */
public class BottomSheetController: UIViewController {
    private var topConstraint: NSLayoutConstraint?
    private var lastPanDirection: UIPanGestureRecognizer.Direction?

    /// The actual `BottomSheet` which can be manipulated
    public var bottomSheetView: UIView & BottomSheet {
        didSet {
            configureView()
        }
    }

    /// The content of the `BottomSheet`
    public var contentView: UIView? {
        get {
            return bottomSheetView.contentView
        }
        set {
            bottomSheetView.contentView = newValue
        }
    }

    /// Delegate that will be informed of state changes
    public var delegate: BottomSheetDelegate?

    private lazy var customTransitionDelegate = TransitionDelegate(initialState: state)

    /**
     Initialize the ViewController
     - Parameter initialState: to be used for the view
     - Parameter viewClosure: optional parameter through which you can supply a different
     view adhering to the `BottomSheet` protocol.
     */
    public init(initialState: BottomSheetView.State = .collapsed,
                viewClosure: ((SheetState) -> UIView & BottomSheet) = { state in BottomSheetView(initialState: state) }) {
        bottomSheetView = viewClosure(initialState)
        super.init(nibName: nil, bundle: nil)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        bottomSheetView = BottomSheetView(initialState: .collapsed)
        super.init(coder: aDecoder)
        configure()
    }

    func configure() {
        modalPresentationStyle = .overCurrentContext
        transitioningDelegate = customTransitionDelegate
    }

    public override func loadView() {
        configureView()
    }

    private func configureView() {
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(BottomSheetController.panGesture))
        bottomSheetView.containerView.addGestureRecognizer(gesture)
        bottomSheetView.delegate = self
        view = bottomSheetView
    }

    @objc
    private func panGesture(recognizer: UIPanGestureRecognizer) {
        let containerView = bottomSheetView.containerView
        let translation = recognizer.translation(in: containerView)
        let newY = containerView.frame.minY + translation.y

        defer { recognizer.setTranslation(.zero, in: containerView) }

        switch recognizer.state {
        case .began, .changed:
            let frame = containerView.frame, contentFrame = bottomSheetView.contentView?.frame ?? .zero, bounds = UIScreen.main.bounds
            let height = max(contentFrame.height, frame.height)
            let maxOffSet = bounds.height - height
            let panDirection = recognizer.direction(in: containerView)

            let safeguard: CGFloat
            if #available(iOS 11.0, *) {
                safeguard = view.safeAreaInsets.top
            } else {
                safeguard = topLayoutGuide.length
            }

            lastPanDirection = [.up, .down].contains(panDirection) ? panDirection : lastPanDirection

            guard newY >= maxOffSet, newY > safeguard else {
                recognizer.reset()
                return
            }
            bottomSheetView.topOffset += translation.y
        case .ended:
            defer { lastPanDirection = nil }

            if case .up? = lastPanDirection {
                return bottomSheetView.expand()
            }
            else if case .down? = lastPanDirection {
                dismissController(animated: true)
            }
        default:
            return
        }
    }
}

// MARK: - BottomSheetDelegate
extension BottomSheetController: BottomSheetDelegate {
    public func changed(to state: State) {
        delegate?.changed(to: state)
    }

    public func dismissed() {
        dismissController(animated: false)
    }
}

// MARK: - Convenience Methods
private extension BottomSheetController {
    func dismissController(animated: Bool = true) {
        dismiss(animated: animated) { [weak delegate] in
            delegate?.dismissed()
        }
    }
}
