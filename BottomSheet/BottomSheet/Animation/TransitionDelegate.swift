//
//  TransitionDelegate.swift
//  BottomSheet
//
//  Created by Daneo Van Overloop on 27/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import Foundation

class TransitionDelegate: NSObject, UIViewControllerTransitioningDelegate {
    private let initialState: BottomSheet.State

    init(initialState: BottomSheet.State) {
        self.initialState = initialState
    }

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentAnimator(initialState: initialState)
    }

    func animationController(forDismissed _: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissAnimator()
    }
}
