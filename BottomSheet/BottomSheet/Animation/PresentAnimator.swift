//
//  PresentAnimator.swift
//  BottomSheet
//
//  Created by Stijn Adams on 29/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import Foundation

class PresentAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    private let initialState: BottomSheet.State

    init(initialState: BottomSheet.State) {
        self.initialState = initialState
    }

    func transitionDuration(using _: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.25
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let sheetController = transitionContext.viewController(forKey: .to) as? BottomSheetController, let bottomSheetView = sheetController.view else {
            transitionContext.completeTransition(true)
            return
        }

        let containerView = transitionContext.containerView
        bottomSheetView.frame = containerView.bounds
        containerView.addSubview(bottomSheetView)

        sheetController.prepareForPresentation()
        bottomSheetView.setNeedsLayout()
        bottomSheetView.layoutIfNeeded()

        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: { [weak self, weak sheetController, weak bottomSheetView] in
            guard let self = self, let sheetController = sheetController, let bottomSheetView = bottomSheetView else { return }

            sheetController.change(to: self.initialState, animated: false, completion: {})
            bottomSheetView.setNeedsLayout()
            bottomSheetView.layoutIfNeeded()
        }, completion: { finished in
            transitionContext.completeTransition(finished)
        })
    }
}
