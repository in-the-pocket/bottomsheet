//
//  DismissAnimator.swift
//  BottomSheet
//
//  Created by Daneo Van Overloop on 26/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import Foundation

class DismissAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using _: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.25
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let sheetController = transitionContext.viewController(forKey: .from) as? BottomSheetController else {
            transitionContext.completeTransition(true)
            return
        }

        sheetController.dismissSheet(animated: true, completion: { finished in
            transitionContext.completeTransition(finished)
        })
    }
}
