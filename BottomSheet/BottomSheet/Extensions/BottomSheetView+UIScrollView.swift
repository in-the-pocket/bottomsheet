//
//  BottomSheetView+UIScrollView.swift
//  BottomSheet
//
//  Created by Daneo Van Overloop on 26/02/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import Foundation

extension BottomSheetView: UIScrollViewDelegate {
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let animateTo = { (state: State) in
            scrollView.isScrollEnabled = false
            self.change(to: state, completion: { self.scrollView.isScrollEnabled = true })
        }

        if case .collapsed = state {
            animateTo(.expanded)
        }
    }

    public func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset _: UnsafeMutablePointer<CGPoint>) {
        guard shouldDismiss(forDraggingVelocity: velocity) else {
            return
        }

        dismiss()
    }

    private func shouldDismiss(forDraggingVelocity velocity: CGPoint) -> Bool {
        return scrollView.isAtTop && state == .expanded && velocity.y < -1
    }
}
