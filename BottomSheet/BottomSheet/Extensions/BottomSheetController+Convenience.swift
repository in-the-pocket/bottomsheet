//
//  BottomSheetController+Convenience.swift
//  BottomSheet
//
//  Created by Daneo Van Overloop on 27/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import Foundation

public extension BottomSheetController {
    /// State of the `BottomSheetView`
    var state: SheetState {
        return bottomSheetView.state
    }

    /**
     Alter the view's state
     - Parameter to: destination state
     */
    func change(to state: SheetState) {
        change(to: state, completion: {})
    }

    /**
     Alter the view's state
     - Parameter to: destination state
     - Parameter completion: block to execute after animating
     */
    func change(to state: SheetState, animated: Bool = true, completion: @escaping () -> Void) {
        bottomSheetView.change(to: state, animated: animated, completion: completion)
    }

    /**
     Dismiss the sheet
     - Parameter animated: boolean indicating whether the dismissal is animated
     - Parameter completion: block to execute after dismissal
     */
    func dismissSheet(animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        bottomSheetView.dismiss(animated: animated, completion: completion)
    }

    /**
     Prepares the sheet for presentation
     */
    func prepareForPresentation() {
        bottomSheetView.prepareForPresentation()
    }
}
