//
//  UIView+RoundedCorners.swift
//  BottomSheet
//
//  Created by Daneo Van Overloop on 26/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import Foundation

extension UIView {
    /**
     These radii will be applied to the top right and left corner
     - Attention: This applies a mask and needs an already laid-out view to work
     */
    func round(corners: UIRectCorner, radii: CGSize) {
        let rectShape = CAShapeLayer()
        rectShape.bounds = frame
        rectShape.position = center
        rectShape.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: radii).cgPath

        layer.mask = rectShape
    }
}
