//
//  UIWindow+SafeAreaLayout.swift
//  BottomSheet
//
//  Created by Stijn Adams on 19/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import Foundation

extension UIWindow {
    var safeAreaInsetsWrapper: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return safeAreaInsets
        } else {
            return .zero
        }
    }
}
