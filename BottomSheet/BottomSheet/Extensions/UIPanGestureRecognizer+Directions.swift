//
//  UIPanGestureRecognizer+Directions.swift
//  BottomSheet
//
//  Created by Stijn Adams on 19/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//
//  Based on a Stackoverflow post by dvdblk
//  (https://stackoverflow.com/a/42650925)
//

import Foundation

extension UIPanGestureRecognizer {

    struct Direction: OptionSet {
        public let rawValue: UInt8

        public init(rawValue: UInt8) {
            self.rawValue = rawValue
        }

        static let up = Direction(rawValue: 1 << 0)
        static let down = Direction(rawValue: 1 << 1)
    }

    func direction(in view: UIView) -> Direction {
        let translation = self.translation(in: view)

        return direction(for: translation.y, greater: .down, lower: .up)
    }
}

// MARK: - Convenience Methods
extension UIPanGestureRecognizer {
    func direction(for translation: CGFloat, greater: Direction, lower: Direction, treshold: CGFloat = 0.5) -> Direction {
        guard abs(translation) > treshold else {
            return []
        }
        return translation > 0 ? greater : lower
    }
}
