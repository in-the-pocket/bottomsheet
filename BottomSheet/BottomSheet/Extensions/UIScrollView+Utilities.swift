//
//  UIScrollView+Utilities.swift
//  BottomSheet
//
//  Created by Daneo Van Overloop on 26/02/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import UIKit

extension UIScrollView {
    var isAtTop: Bool {
        return contentOffset.y <= 0
    }
}
