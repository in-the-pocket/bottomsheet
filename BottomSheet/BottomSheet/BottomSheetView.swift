//
//  BottomSheetView.swift
//  BottomSheet
//
//  Created by Daneo Van Overloop on 13/02/2019.
//  Copyright © 2019 Swiftin. All rights reserved.
//

import Anchorage
import Foundation
import UIKit

/// Enum defining the possible states of the view
public enum SheetState {
    /// Collapsed (48 points (+ `safeAreaInsets` if applicable))
    case collapsed
    /// Expands to either the contentView's `intrinsicContentSize` or the `topLayoutGuide`
    case expanded
}

/// Container for the content to be displayed in the shet
/// `BottomSheet` implementation
public class BottomSheetView: UIView, BottomSheet {
    /// The container for the contentView
    public var containerView: UIView = UIView()

    /// The view to be displayed in the sheet
    public var contentView: UIView? {
        get {
            return scrollView.contentView
        }
        set {
            scrollView.contentView = newValue
        }
    }

    /// OffSet used by the PanGesture to animate the view up or downwards
    public var topOffset: CGFloat {
        get {
            return topConstraint?.constant ?? 0
        }
        set {
            topConstraint?.constant = newValue
            backgroundView.alpha = alpha(for: state, offset: topOffset)
        }
    }

    /// View's background color
    public override var backgroundColor: UIColor? {
        get {
            return scrollView.backgroundColor
        }
        set {
            containerView.backgroundColor = newValue
            scrollView.backgroundColor = newValue
        }
    }

    /// These radii will be applied to the top right and left corner
    public var cornerRadii: CGSize = CGSize(width: 15, height: 15) {
        didSet {
            containerView.round(corners: [.topRight, .topLeft], radii: cornerRadii)
        }
    }

    /// Current state of the sheet
    public private(set) var state: State {
        didSet {
            updateArrowButton(for: state)
        }
    }

    /// Delegate that will be informed of state changes
    public var delegate: BottomSheetDelegate?

    private var topConstraint: NSLayoutConstraint?

    lazy var arrowButton: UIButton = {
        let button = UIButton(type: .system)

        button.accessibilityIdentifier = BottomSheetView.dismissButtonIdentifier
        button.addTarget(self, action: #selector(didTapArrowButton), for: .touchUpInside)

        return button
    }()

    private var layoutGuide: UILayoutGuide = {
        let guide = UILayoutGuide()
        guide.identifier = "BottomSheet LayoutGuide"
        return guide
    }()

    private var peak: CGFloat {
        return 48 + (window?.safeAreaInsetsWrapper.bottom ?? 0)
    }

    let scrollView = SheetScrollView()

    private let backgroundView: UIView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let visualEffect = UIVisualEffectView(effect: blurEffect)
        visualEffect.backgroundColor = .black
        visualEffect.accessibilityIdentifier = BottomSheetView.backgroundViewIdentifier

        return visualEffect
    }()

    /// Default images used by the sheet for the different states
    public var images: (scrollDownImage: UIImage?, scrollUpImage: UIImage?) = {
        let bundle = Bundle(for: BottomSheetView.self)
        let downImage = UIImage(named: "chevron_down", in: bundle, compatibleWith: nil)
        let upImage = UIImage(named: "chevron_up", in: bundle, compatibleWith: nil)
        return (downImage, upImage)
    }() {
        didSet {
            updateArrowButton(for: state)
        }
    }

    /**
     Initialize and configure the BottomSheetView for the given state
     - Parameter initialState: used to configure this view accordingly
     - SeeAlso: `BottomSheet.init(initialState:)`
     */
    public required init(initialState: State) {
        state = initialState
        super.init(frame: .infinite)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        state = .collapsed
        super.init(coder: aDecoder)
        configure()
    }

    func configure() {
        configureViews()
        configureIdentifiers()
        configureConstraints()

        change(to: state, animated: false, completion: {})
    }

    func configureViews() {
        [backgroundView, containerView].forEach { addSubview($0) }
        [arrowButton, scrollView].forEach { containerView.addSubview($0) }
        addLayoutGuide(layoutGuide)

        containerView.backgroundColor = .white

        updateArrowButton(for: state)
        configureScrollView()
        configureBlurryBackground()
    }

    private func configureScrollView() {
        scrollView.delegate = self
        scrollView.backgroundColor = .white
    }

    private func configureBlurryBackground() {
        backgroundView.alpha = alpha(for: state)
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(lightDismiss)))
    }

    public func prepareForPresentation() {
        topConstraint?.isActive = false
        topConstraint = containerView.topAnchor == layoutGuide.bottomAnchor
        backgroundView.alpha = 0
    }

    private func updateArrowButton(for state: State) {
        let arrowImage = arrow(for: state)
        arrowButton.setImage(arrowImage?.withRenderingMode(.alwaysOriginal), for: .normal)
    }

    public func dismiss(animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        topConstraint?.isActive = false

        let animationBlock: () -> Void = { [weak self, weak containerView] in
            guard let self = self, let containerView = containerView else {
                return
            }

            self.topConstraint = containerView.topAnchor == self.layoutGuide.bottomAnchor
            self.backgroundView.alpha = 0
        }
        let completionBlock: (Bool) -> Void = { [weak self] finished in
            completion?(finished)
            self?.delegate?.dismissed()
        }

        if animated {
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                animationBlock()
                self?.setNeedsLayout()
                self?.layoutIfNeeded()
            }, completion: completionBlock)
        } else {
            animationBlock()
            completionBlock(true)
        }
    }

    /**
     Animate to the supplied state
     - Parameter to: destination state to animate to
     - Parameter completion: block executed at the end of animation
     - SeeAlso: `BottomSheet.change(to:completion:)`
     */
    public func change(to state: State, animated: Bool = true, completion: @escaping () -> Void = {}) {
        topConstraint?.isActive = false

        let animationBlock: () -> Void = { [weak self, weak containerView, layoutGuide] in
            guard let self = self, let containerView = containerView else {
                return
            }

            switch state {
            case .collapsed:
                self.topConstraint = containerView.topAnchor == layoutGuide.bottomAnchor - self.peak
            case .expanded:
                self.topConstraint = containerView.topAnchor == layoutGuide.topAnchor
            }

            self.backgroundView.alpha = self.alpha(for: state)
        }
        let completionBlock: (Bool) -> Void = { [weak self, weak scrollView, delegate] _ in
            self?.state = state
            completion()
            scrollView?.isScrollEnabled = true
            delegate?.changed(to: state)
        }

        if animated {
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                animationBlock()
                self?.setNeedsLayout()
                self?.layoutIfNeeded()
            }, completion: completionBlock)
        } else {
            animationBlock()
            completionBlock(true)
        }
    }

    func offsetPercentage(for state: State, offset: CGFloat) -> CGFloat {
        let minOffset = minimumOffset(for: state)
        let maxOffset = maximumOffset(for: state)
        let currentOffset = abs(offset)

        let percentage = (currentOffset - minOffset) / (maxOffset - minOffset)
        let boundedPercentage = max(min(1, percentage), 0)

        return boundedPercentage
    }

    @objc
    private func didTapArrowButton() {
        if case .expanded = self.state { self.dismiss() }
        else if case .collapsed = self.state { self.expand() }
    }
}

// MARK: - Lifecycle overrides

extension BottomSheetView {
    /**
     Adds a view as subview

     - Parameter view: will be used as content view for the BottomSheetView
     - Attention: Calling this method will result in the content view being replaced with the supplied view
     */
    public override func addSubview(_ view: UIView) {
        if view === containerView || view === backgroundView {
            super.addSubview(view)
            return
        }

        contentView = view
        configureIdentifiers()
    }

    private func configureConstraints() {
        layoutGuide.horizontalAnchors == horizontalAnchors
        layoutGuide.bottomAnchor == bottomAnchor
        layoutGuide.heightAnchor == containerView.heightAnchor ~ .low

        if #available(iOS 11.0, *) {
            layoutGuide.topAnchor >= safeAreaLayoutGuide.topAnchor
        } else {
            layoutGuide.topAnchor >= topAnchor
        }

        topConstraint = containerView.topAnchor == layoutGuide.bottomAnchor
        backgroundView.edgeAnchors == edgeAnchors

        arrowButton.topAnchor == containerView.topAnchor + 8
        arrowButton.horizontalAnchors == horizontalAnchors
        arrowButton.heightAnchor == 30

        containerView.horizontalAnchors == scrollView.horizontalAnchors
        containerView.bottomAnchor == scrollView.bottomAnchor
        scrollView.topAnchor == arrowButton.bottomAnchor

        scrollView.widthAnchor == widthAnchor
        scrollView.heightAnchor <= heightAnchor
        scrollView.horizontalAnchors == horizontalAnchors
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        containerView.round(corners: [.topRight, .topLeft], radii: cornerRadii)
    }
}

// MARK: - State view specific

extension BottomSheetView {
    func alpha(for state: State, offset: CGFloat = 0) -> CGFloat {
        let percentage = offsetPercentage(for: state, offset: offset)

        switch state {
        case .expanded: return 0.5 * (1.0 - percentage)
        case .collapsed: return 0.5 * percentage
        }
    }

    private func minimumOffset(for state: State) -> CGFloat {
        switch state {
        case .collapsed: return peak
        case .expanded: return 0
        }
    }

    private func maximumOffset(for state: State) -> CGFloat {
        switch state {
        case .collapsed: return containerView.frame.size.height
        case .expanded: return containerView.frame.size.height - peak
        }
    }

    func arrow(for state: State) -> UIImage? {
        guard case .expanded = state else {
            return images.scrollUpImage
        }

        return images.scrollDownImage
    }
}

// MARK: - State Manipulation

private extension BottomSheetView {
    @objc
    func lightDismiss() {
        guard case .expanded = state else {
            return
        }
        dismiss()
    }
}

// MARK: - Convenience Methods
extension BottomSheetView {
    var arrowImage: UIImage? {
        return arrowButton.image(for: .normal)
    }
}
