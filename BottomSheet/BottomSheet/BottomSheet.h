//
//  BottomSheet.h
//  BottomSheet
//
//  Created by Daneo Van Overloop on 19/02/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BottomSheet.
FOUNDATION_EXPORT double BottomSheetVersionNumber;

//! Project version string for BottomSheet.
FOUNDATION_EXPORT const unsigned char BottomSheetVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BottomSheet/PublicHeader.h>

