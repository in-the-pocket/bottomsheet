//
//  UtilTests.swift
//  BottomSheetTests
//
//  Created by Stijn Adams on 20/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import XCTest

class UtilTests: XCTestCase {
    func testCombine() {
        let testCases: [TestCase<Int, String>] = [
            TestCase<Int, String>(left: [], right: ["A", "B", "C"], expectedElements: Set<Pair>([]), expectedCount: 0),
            TestCase<Int, String>(left: [1, 2, 3], right: [], expectedElements: Set<Pair>([]), expectedCount: 0),
            TestCase<Int, String>(left: [1, 2], right: ["A"], expectedElements: Set<Pair>([Pair(1, "A"), Pair(2, "A")]), expectedCount: 2),
            TestCase<Int, String>(left: [1], right: ["A", "B"], expectedElements: Set<Pair>([Pair(1, "A"), Pair(1, "B")]), expectedCount: 2),
            TestCase<Int, String>(left: [1, 2], right: ["A", "B"], expectedElements: Set<Pair>([Pair(1, "A"), Pair(1, "B"), Pair(2, "A"), Pair(2, "B")]), expectedCount: 4)
        ]

        testCases.forEach { (testCase) in
            let result = testCase.execute()

            XCTAssertEqual(testCase.expectedCount, result.count)
            testCase.expectedElements.forEach { (expectedElement) in
                XCTAssertTrue(result.contains(where: { $0 == expectedElement }))
            }
        }
    }
}

private struct TestCase<S, T> where S: Hashable, T: Hashable {
    let left: [S]
    let right: [T]
    let expectedElements: Set<Pair<S, T>>
    let expectedCount: Int

    func execute() -> Set<Pair<S, T>> {
        return Set(left.combine(right, { Pair($0, $1) }))
    }
}

private struct Pair<S, T>: Hashable where S: Hashable, T: Hashable {
    let left: S
    let right: T

    init(_ l: S, _ r: T) {
        self.left = l
        self.right = r
    }
}
