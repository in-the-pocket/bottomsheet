//
//  Util.swift
//  BottomSheet
//
//  Created by Stijn Adams on 20/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import Foundation

extension Array {

    func combine<T>(_ other: [T]) -> [(Element, T)] {
        return combine(other, { return ($0, $1) })
    }

    func combine<T, Result>(_ other: [T], _ transform: @escaping (Element, T) -> Result) -> [Result] {
        return reduce([], { (acc, el) -> [Result] in
            return acc + other.map({ transform(el, $0) })
        })
    }
}
