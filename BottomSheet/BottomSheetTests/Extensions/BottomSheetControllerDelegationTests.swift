//
//  BottomSheetControllerDelegationTests.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 27/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import BottomSheet
import Foundation
import XCTest

class BottomSheetControllerDelegationTests: XCTestCase {
    var changedState: SheetState?
    var isDismissed: Bool = false
    lazy var stub = { prepareMock(startState: .collapsed) }()

    func testChangingControllerStatePropagatesToSheet() {
        stub.controller.change(to: .expanded)
        XCTAssertEqual(stub.sheet.state, SheetState.expanded)
    }

    func testControllerStateReflectsSheetState() {
        stub.sheet.change(to: .expanded)
        XCTAssertEqual(stub.controller.state, stub.sheet.state)
    }

    func testDelegateIsCalledOnChangedState() {
        stub.controller.delegate = self
        stub.controller.preloadView()
        stub.sheet.change(to: .expanded, animated: false)
        XCTAssertEqual(changedState, .expanded)
    }

    func testDelegateIsCalledOnDismissal() {
        stub.controller.delegate = self
        stub.controller.preloadView()
        stub.sheet.dismiss(animated: false)

        XCTAssertTrue(isDismissed)
    }

    func testChangingContentViewPropogatesToSheet() {
        let referenceView = UIView()
        stub.controller.contentView = referenceView

        XCTAssertEqual(stub.sheet.contentView, referenceView)
        XCTAssertEqual(stub.sheet.contentView, stub.controller.contentView)
    }
}

extension BottomSheetControllerDelegationTests: BottomSheetDelegate {
    func changed(to: State) {
        changedState = to
    }

    func dismissed() {
        isDismissed = true
    }
}

extension BottomSheetControllerDelegationTests {
    private func prepareMock(startState: SheetState) -> (controller: TestBottomSheetController, sheet: MockSheet) {
        let sheet = MockSheet(initialState: startState)
        let controller = TestBottomSheetController(initialState: startState, viewClosure: { _ in sheet })

        return (controller, sheet)
    }
}
