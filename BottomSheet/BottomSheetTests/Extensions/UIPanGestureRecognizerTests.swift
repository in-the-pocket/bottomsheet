//
//  UIPanGestureRecognizerTests.swift
//  BottomSheetTests
//
//  Created by Stijn Adams on 19/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import XCTest
@testable import BottomSheet

class UIPanGestureRecognizerTests: XCTestCase {
    typealias Direction = UIPanGestureRecognizer.Direction

    func testDirectionInOneDimension() {
        let panGesture = UIPanGestureRecognizer()
        let cases: [(velocity: CGFloat, greater: Direction, lower: Direction, expectedResult: Direction)] = [
            (velocity: 1, greater: .down, lower: .up, expectedResult: .down),
            (velocity: -1, greater: .down, lower: .up, expectedResult: .up),
            (velocity: 0, greater: .down, lower: .up, expectedResult: []),
        ]

        cases.forEach { (velocity, greater, lower, expectedResult) in
            XCTAssertEqual(panGesture.direction(for: velocity, greater: greater, lower: lower), expectedResult)
        }
    }
}
