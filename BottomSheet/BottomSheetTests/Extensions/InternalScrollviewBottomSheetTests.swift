//
//  InternalBottomSheetTests.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 19/02/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

@testable import BottomSheet
import Foundation
import XCTest

extension BottomSheetTests {
    func testPullingDownAtTopOfScrollViewCollapses() {
        let expectedResult = expectation(description: "Dismisses sheet")

        dismissalClosure = { isDismissed in
            XCTAssertTrue(isDismissed)
            expectedResult.fulfill()
        }

        let scrollView = UIScrollView()
        scrollView.contentOffset = CGPoint(x: 0, y: -15) // Mimic having the view pulled down

        expandedSheet.scrollViewWillEndDragging(scrollView,
                                                withVelocity: CGPoint(x: 0, y: -10),
                                                targetContentOffset: UnsafeMutablePointer<CGPoint>.allocate(capacity: 0))

        wait(for: [expectedResult], timeout: 0.5)
    }

    func testScrollingExpandsAndDisablesView() {
        let expectedResult = expectation(description: "Expanded on scroll")

        validationClosure = { state in
            XCTAssertEqual(state, SheetState.expanded)
            expectedResult.fulfill()
        }

        let scrollView = UIScrollView()
        collapsedSheet.scrollViewWillBeginDragging(scrollView)

        XCTAssertFalse(scrollView.isScrollEnabled)
        wait(for: [expectedResult], timeout: 0.5)
    }

    func testScrollingExpandedViewDoesNotDisable() {
        let expectedResult = expectation(description: "Does nothing on scroll")
        expectedResult.isInverted = true

        validationClosure = { state in
            XCTAssertEqual(state, SheetState.expanded)
            expectedResult.fulfill()
        }

        let scrollView = UIScrollView()
        expandedSheet.scrollViewWillBeginDragging(scrollView)

        XCTAssertTrue(scrollView.isScrollEnabled)
        wait(for: [expectedResult], timeout: 0.5)
    }

    func testScrollingExpandedViewToTopDoesNotCollapse() {
        let expectedResult = expectation(description: "Dismisses sheet")
        expectedResult.isInverted = true

        validationClosure = { _ in
            XCTFail("We shouldn't have animated")
            expectedResult.fulfill()
        }

        expandedSheet.scrollViewWillEndDragging(UIScrollView(),
                                                withVelocity: CGPoint(x: 0, y: 0),
                                                targetContentOffset: UnsafeMutablePointer<CGPoint>.allocate(capacity: 0))

        wait(for: [expectedResult], timeout: 0.5)
    }
}

private extension BottomSheetTests {
    var collapsedSheet: BottomSheetView {
        let sheet = BottomSheetView(initialState: .collapsed)
        sheet.delegate = self
        return sheet
    }

    var expandedSheet: BottomSheetView {
        let sheet = BottomSheetView(initialState: .expanded)
        sheet.delegate = self
        return sheet
    }
}

extension BottomSheetTests: BottomSheetDelegate {
    func changed(to state: State) {
        validationClosure?(state)
    }

    func dismissed() {
        dismissalClosure?(true)
    }
}
