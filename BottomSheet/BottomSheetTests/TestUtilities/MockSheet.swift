//
//  MockSheet.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 26/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

@testable import BottomSheet
import Foundation

class MockSheet: UIView, BottomSheet {
    var containerView: UIView = UIView()

    var images: (scrollDownImage: UIImage?, scrollUpImage: UIImage?) = (nil, nil)

    var topOffset: CGFloat = 0

    var state: State

    var contentView: UIView?

    var delegate: BottomSheetDelegate?

    required init(initialState: State) {
        state = initialState
        super.init(frame: .zero)
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func change(to newState: State, animated: Bool = true, completion: @escaping () -> Void) {
        state = newState
        completion()
        delegate?.changed(to: newState)
    }

    func dismiss(animated: Bool, completion: ((Bool) -> Void)?) {
        completion?(true)
        delegate?.dismissed()
    }

    func prepareForPresentation() {
    }
}
