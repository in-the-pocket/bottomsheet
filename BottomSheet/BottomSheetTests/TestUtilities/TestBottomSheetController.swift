//
//  TestBottomSheetController.swift
//  BottomSheetTests
//
//  Created by Stijn Adams on 05/04/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import Foundation
@testable import BottomSheet

class TestBottomSheetController: BottomSheetController {
    func preloadView() {
        _ = view
    }

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        completion?()
    }
}
