//
//  BottomSheetControllerTests.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 20/02/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

@testable import BottomSheet
import Foundation
import XCTest

class BottomSheetControllerTests: XCTestCase {
    let controller = BottomSheetController(initialState: .collapsed)

    func testCorrectlyInitializseWithCoder() {
        let decoder = MockControllerDecoder()
        XCTAssertNoThrow(BottomSheetController(coder: decoder))
    }

    func testViewReflectsInitialState() {
        let state: [SheetState] = [.collapsed, .expanded]
        let mockViewClosure = { (state: SheetState) in MockSheet(initialState: state) }
        state.forEach { initialState in
            let controller = BottomSheetController(initialState: initialState, viewClosure: mockViewClosure)
            XCTAssertEqual(controller.bottomSheetView.state, initialState)
        }
    }

    func testControllerConfiguresAsDelegateToSheet() {
        let mockView = MockSheet(initialState: .collapsed)
        controller.bottomSheetView = mockView
        XCTAssertEqual(mockView.delegate as? BottomSheetController, controller)
    }

    func testBottomSheetViewAssignedAsMainView() {
        controller.loadView()
        XCTAssertEqual(controller.view, controller.bottomSheetView)
    }

    func testDisplaysOverCurrentContextAsModal() {
        XCTAssertEqual(controller.modalPresentationStyle, UIModalPresentationStyle.overCurrentContext)
    }
}

extension BottomSheetControllerTests: BottomSheetDelegate {
    func changed(to _: State) {}
    func dismissed() {}
}

/// This is a private (stub) implemntation in order to test the `init(coder: NSCoder)` initializer correctly returns
private class MockControllerDecoder: NSCoder {
    /// Returns stub items for required fields and nil otherwise
    override func decodeObject(forKey key: String) -> Any? {
        switch key {
        case "UITitle":
            return "BottomSheetController"
        case "UIView":
            return UIView()
        case "UINavigationItem":
            return UINavigationItem(title: "Hello")
        default:
            return nil
        }
    }

    /// return: false on whether we want fullscreen, true otherwise
    override func decodeBool(forKey key: String) -> Bool {
        return key != "UIWantsFullScreenLayout"
    }

    ///
    override func containsValue(forKey _: String) -> Bool {
        return false
    }
}

// UINavigationItem
// UIParentViewController
