//
//  SheetScrollViewTests.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 07/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

@testable import BottomSheet
import Foundation
import XCTest

class ScrollSheetTests: XCTestCase {
    let scrollSheet = SheetScrollView()

    func testSettingContentViewAddsItAsSubview() {
        let contentView = UIView()

        XCTAssertNil(scrollSheet.contentView)
        scrollSheet.contentView = contentView

        XCTAssertEqual(scrollSheet, contentView.superview)
    }

    func testSettingContentViewReplacesExistingContentView() {
        let previousContentView = UIView()
        scrollSheet.contentView = previousContentView
        XCTAssertEqual(previousContentView, scrollSheet.contentView)

        let newContentView = UIView()
        scrollSheet.contentView = newContentView
        XCTAssertEqual(newContentView, scrollSheet.contentView)
    }

    func testAddingSubviewDoesntImpactContentView() {
        let contentView = UIView()
        scrollSheet.contentView = contentView
        scrollSheet.addSubview(UIView())
        XCTAssertEqual(scrollSheet.contentView, contentView)
    }

    func testRemovingContentViewRemovesItFromViewHierarcy() {
        let previousContentView = UIView()
        scrollSheet.contentView = previousContentView
        XCTAssertEqual(previousContentView, scrollSheet.contentView)

        scrollSheet.contentView = nil
        XCTAssertNil(previousContentView.superview)
        XCTAssertNil(scrollSheet.contentView)
    }

    func testCustomConfiguration() {
        XCTAssertFalse(scrollSheet.delaysContentTouches)
        XCTAssertFalse(scrollSheet.showsHorizontalScrollIndicator)
    }
}

extension SheetScrollView {
    convenience init() {
        self.init(frame: .zero)
    }
}
