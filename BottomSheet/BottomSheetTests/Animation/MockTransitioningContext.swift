//
//  MockTransitioningContext.swift
//  BottomSheetTests
//
//  Created by Stijn Adams on 02/04/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import UIKit

class MockTransitioningContext: NSObject, UIViewControllerContextTransitioning {
    var containerView = UIView()
    var isAnimated = true
    var isInteractive = false
    var transitionWasCancelled = false
    var presentationStyle: UIModalPresentationStyle = .overCurrentContext
    var completedTransition = false

    private var fromController: UIViewController?
    private var toController: UIViewController?

    init(from fromController: UIViewController? = nil, to toController: UIViewController? = nil) {
        self.fromController = fromController
        self.toController = toController
    }

    func viewController(forKey key: UITransitionContextViewControllerKey) -> UIViewController? {
        switch key {
        case .from: return fromController
        case .to:   return toController
        default:    return nil
        }
    }

    // Mandatory protocol conformance
    func updateInteractiveTransition(_: CGFloat) {}

    func finishInteractiveTransition() {}

    func cancelInteractiveTransition() {
        transitionWasCancelled = true
    }

    func pauseInteractiveTransition() {}

    func completeTransition(_ didComplete: Bool) {
        completedTransition = didComplete
    }

    func view(forKey _: UITransitionContextViewKey) -> UIView? {
        return nil
    }

    var targetTransform: CGAffineTransform = .identity

    func initialFrame(for _: UIViewController) -> CGRect {
        return .zero
    }

    func finalFrame(for _: UIViewController) -> CGRect {
        return .zero
    }
}
