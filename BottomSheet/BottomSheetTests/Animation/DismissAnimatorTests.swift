//
//  DismissAnimatorTests.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 26/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

@testable import BottomSheet
import Foundation
import XCTest

class DismissAnimatorTests: XCTestCase {
    let animator = DismissAnimator()
    private var isDismissed = false
    private var dismissalClosure: ((Bool) -> Void)?

    func testDuration() {
        XCTAssertEqual(animator.transitionDuration(using: nil), 0.25)
    }

    func testDismissesProperly() {
        let sheet = MockSheet(initialState: .expanded)
        let controller = TestBottomSheetController(initialState: .expanded, viewClosure: { _ in sheet })
        let mockContext = MockTransitioningContext(from: controller)

        controller.delegate = self
        controller.preloadView()

        let expectedResult = expectation(description: "Dismiss properly with animator")

        dismissalClosure = { isDismissed in
            XCTAssertTrue(isDismissed)
            XCTAssertTrue(mockContext.completedTransition)
            expectedResult.fulfill()
        }
        animator.animateTransition(using: mockContext)

        wait(for: [expectedResult], timeout: 0.5)
    }

    func testCompletesTransitionOnIncorrectViewController() {
        let mockContext = MockTransitioningContext(from: UIViewController())
        animator.animateTransition(using: mockContext)
        XCTAssertTrue(mockContext.completedTransition)
    }
}

extension DismissAnimatorTests: BottomSheetDelegate {
    func changed(to: State) {}
    func dismissed() {
        dismissalClosure?(true)
    }
}
