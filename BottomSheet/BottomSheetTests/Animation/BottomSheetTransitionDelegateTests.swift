//
//  BottomSheetTransitionDelegateTEsts.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 27/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

@testable import BottomSheet
import Foundation
import XCTest

class BottomSheetTransitionDelegateTests: XCTestCase {
    func testReturnsProperAnimationObject() {
        let transitionDelegate = TransitionDelegate(initialState: .collapsed)
        let animator = transitionDelegate.animationController(forDismissed: UIViewController())

        XCTAssertTrue(animator is DismissAnimator)
    }
}
