//
//  PresentAnimatorTests.swift
//  BottomSheetTests
//
//  Created by Stijn Adams on 01/04/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

@testable import BottomSheet
import Foundation
import XCTest

class PresentAnimatorTests: XCTestCase {

    func testDuration() {
        let animator = PresentAnimator(initialState: .collapsed)
        XCTAssertEqual(animator.transitionDuration(using: nil), 0.25)
    }

    func testPresentsProperly() {
        let testCases: [SheetState] = [
            .collapsed,
            .expanded
        ]

        testCases.forEach { state in
            let animator = PresentAnimator(initialState: state)
            let sheetController = BottomSheetController(initialState: state, viewClosure: { MockSheet(initialState: $0) })
            let mockContext = MockTransitioningContext(to: sheetController)

            let expectedResult = expectation(description: "Present sheet")

            animator.animateTransition(using: mockContext)

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                let sheet = sheetController.bottomSheetView

                XCTAssertEqual(sheet.state, state)
                XCTAssertTrue(mockContext.containerView.subviews.contains(where: { $0 == sheet }))
                XCTAssertTrue(mockContext.completedTransition)
                
                expectedResult.fulfill()
            }
            wait(for: [expectedResult], timeout: 0.3)
        }
    }

    func testCompletesTransitionOnIncorrectViewController() {
        let animator = PresentAnimator(initialState: .collapsed)
        let mockContext = MockTransitioningContext(to: UIViewController())
        animator.animateTransition(using: mockContext)
        XCTAssertTrue(mockContext.completedTransition)
    }
}
