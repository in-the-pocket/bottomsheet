//
//  InternalSheetControllerTests.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 26/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

@testable import BottomSheet
import Foundation
import XCTest

extension BottomSheetControllerTests {
    func testAssignsGestureRecognizer() {
        let controller = BottomSheetController(initialState: .collapsed)
        let activeGestures = { controller.bottomSheetView.containerView.gestureRecognizers ?? [] }
        let initialGestureRecognizers = activeGestures()
        controller.loadView()
        XCTAssertEqual(activeGestures().count, initialGestureRecognizers.count + 1)
    }
}
