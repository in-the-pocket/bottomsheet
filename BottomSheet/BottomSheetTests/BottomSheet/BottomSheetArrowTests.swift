//
//  BottomSheetViewTests.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 05/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

@testable import BottomSheet
import Foundation
import XCTest
import Require

extension BottomSheetTests {
    private var scrollDownImage: UIImage {
        return UIImage(named: "chevron_down", in: frameworkBundle, compatibleWith: nil).require()
    }

    private var scrollDownImageData: Data {
        return scrollDownImage.pngData().require()
    }

    private var scrollUpImage: UIImage {
        return UIImage(named: "chevron_up", in: frameworkBundle, compatibleWith: nil).require()
    }

    private var scrollUpImageData: Data {
        return scrollUpImage.pngData().require()
    }

    private var frameworkBundle: Bundle {
        return Bundle(for: BottomSheetView.self)
    }

    func testDefaultArrowAssetsExist() {
        XCTAssertEqual(sheet.images.scrollDownImage?.pngData(), scrollDownImageData)
        XCTAssertEqual(sheet.images.scrollUpImage?.pngData(), scrollUpImageData)
    }

    func testArrowReflectsState() {
        let collapsedSheet = sheetWithState(.collapsed)
        let expandedSheet = sheetWithState(.expanded)

        XCTAssertEqual(collapsedSheet.arrowImage?.pngData(), scrollUpImageData)
        XCTAssertEqual(expandedSheet.arrowImage?.pngData(), scrollDownImageData)
    }

    func testArrowChangesOnAnimation() {
        let testCases: [(startState: SheetState, endState: SheetState, expectedArrowImage: UIImage)] = [
            (.collapsed, .expanded, scrollDownImage),
            (.expanded, .collapsed, scrollUpImage)
        ]

        testCases.forEach { startState, endState, expectedArrowImage in
            let expectAnimation = expectation(description: "Animation completed")
            let sheet = sheetWithState(startState)
            sheet.change(to: endState) {
                XCTAssertEqual(sheet.arrowImage?.pngData(), expectedArrowImage.pngData().require())
                expectAnimation.fulfill()
            }
            wait(for: [expectAnimation], timeout: 0.3)
        }
    }

    func testReplacingContentViewDoesNotAffectArrow() {
        let testCases: [(state: SheetState, arrowImage: UIImage, previousContentView: UIView?, newContentView: UIView?)] = [
            (.collapsed, scrollUpImage, UIView(), nil),
            (.collapsed, scrollUpImage, nil, UIView()),
            (.expanded, scrollDownImage, nil, UIView()),
            (.expanded, scrollDownImage, UIView(), nil)
        ]

        testCases.forEach { (state, arrowImage, previousContentView, newContentView) in
            let sheet = sheetWithState(state)
            sheet.contentView = previousContentView
            XCTAssertEqual(sheet.arrowImage?.pngData(), arrowImage.pngData().require())

            sheet.contentView = newContentView
            XCTAssertEqual(sheet.arrowImage?.pngData(), arrowImage.pngData().require())
        }
    }
}

private extension BottomSheetTests {
    func sheetWithSuperview(sheetState: SheetState) -> BottomSheetView {
        let sheet = sheetWithState(sheetState)
        let view = UIView()
        view.addSubview(sheet)

        return sheet
    }
}
