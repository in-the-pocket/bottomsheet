//
//  BottomSheetAnimationTests.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 19/02/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

import BottomSheet
import Foundation
import XCTest

extension BottomSheetTests {
    func testAnimationToVariousStates() {
        let states: [State] = [.collapsed, .expanded]

        let mappedStates = states.map({ state in
            (state, states.filter({ $0 != state }))
        })

        mappedStates.forEach { initialState, possibleStates in
            let superview = UIView()
            let originalConstraintsCount = superview.constraints.count
            let sheet = BottomSheetView(initialState: initialState)

            superview.addSubview(sheet)
            possibleStates.forEach({ state in
                validate(sheet: sheet, stateChange: state, closure: {
                    XCTAssertNotEqual(superview.constraints.count, originalConstraintsCount)
                })
            })
        }
    }
}

private extension BottomSheetTests {
    typealias SheetView = BottomSheet & UIView
    func validate(sheet: SheetView, stateChange state: State, closure: @escaping () -> Void) {
        let expected = expectation(description: "Switched to \(state)")
        sheet.change(to: state, animated: true, completion: {
            XCTAssertEqual(sheet.state, state)
            closure()
            expected.fulfill()
        })

        wait(for: [expected], timeout: 0.5)
    }
}
