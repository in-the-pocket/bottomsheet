//
//  InternalBackgroundBottomSheetTests.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 05/03/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

@testable import BottomSheet
import Foundation
import XCTest

extension BottomSheetTests {
    func testChangesAlphaOnAnimation() {
        let possibleStates = states.map({ state in (state, states.filter({ $0 != state })) })
        possibleStates.forEach { state, differentStates in
            differentStates.forEach({ newState in
                verifyAlphaChanges(startingFrom: state, to: newState)
            })
        }
    }

    func testAlphaValueIsCorrectForStates() {
        states.forEach { state in
            let sheet = TestSheet(initialState: state)
            let returnedAlpha = sheet.alpha(for: state, offset: 0)
            switch state {
            case .collapsed:
                XCTAssertEqual(returnedAlpha, 0)
            case .expanded:
                XCTAssertEqual(returnedAlpha, 0.5)
            }
        }
    }

    func testBackgroundColorDelegatesToScrollView() {
        let color: UIColor = .black

        XCTAssertNotEqual(sheet.backgroundColor, color)
        sheet.backgroundColor = color
        XCTAssertEqual(sheet.scrollView.backgroundColor, color)
    }

    func testAddMaskForRoundedCorners() {
        sheet.layoutSubviews()

        guard let roundedCornerShape = sheet.containerView.layer.mask as? CAShapeLayer else {
            XCTFail("We need to mask the corners")
            return
        }

        XCTAssertNotNil(roundedCornerShape)
    }

    func testRoundedCornersAreSet() {
        sheet.cornerRadii = CGSize(width: 10, height: 10)
        XCTAssertNotNil(sheet.containerView.layer.mask)
    }

    func testAddsBackgroundView() {
        XCTAssertTrue(sheet.subviews.first is UIVisualEffectView)
    }
}

extension BottomSheetTests {
    var states: [SheetState] {
        return [.collapsed, .expanded]
    }
}

extension BottomSheetTests {
    func sheetWithSuperview(state: SheetState) -> (UIView, BottomSheetView) {
        let view = UIView(), sheet = sheetWithState(state)
        view.addSubview(sheet)
        return (view, sheet)
    }

    private func verifyAlphaChanges(startingFrom _: State, to state: State) {
        let expected = expectation(description: "Changes alpha value")
        let sheet = self.sheet

        sheet.change(to: .expanded) {
            let backgroundView = sheet.backgroundView ?? UIView()
            XCTAssertEqual(backgroundView.alpha, sheet.alpha(for: .expanded))
            expected.fulfill()
        }

        wait(for: [expected], timeout: 0.1)
    }
}

private extension UIView {
    var backgroundView: UIView? {
        return subviews.filter({ !($0 is BottomSheetView) }).first
    }
}

// MARK: - Dismiss after scrolling tests
extension BottomSheetTests {
    func testDismissesWhenExpanded() {
        let testCases = makeTestCases(initialSheetStates: [.expanded])

        testCases.forEach { (testCase) in
            let sheet = TestSheet(initialState: testCase.state)
            sheet.delegate = self
            sheet.scroll(to: testCase.offset, with: testCase.velocity)

            let expectedResult = expectation(description: "Dismisses sheet")

            dismissalClosure = { isDismissed in
                XCTAssertTrue(isDismissed)
                expectedResult.fulfill()
            }

            wait(for: [expectedResult], timeout: 0.5)
        }
    }

    func testDoesNotDismissWhenCollapsed() {
        let testCases = makeTestCases(initialSheetStates: [.collapsed])

        testCases.forEach { (testCase) in
            let sheet = TestSheet(initialState: testCase.state)
            sheet.scroll(to: testCase.offset, with: testCase.velocity)

            XCTAssertEqual(sheet.state, testCase.state)
        }
    }

    func testDoesNotDismissWhenHasPositiveScrollOffset() {
        let positiveOffsetPoints: [CGPoint] = [
            CGPoint(x: 0, y: 2)
        ]

        let testCases = makeTestCases(initialSheetStates: [.expanded],
                                      offsetPoints: positiveOffsetPoints)

        testCases.forEach { (testCase) in
            let sheet = TestSheet(initialState: testCase.state)
            sheet.scroll(to: testCase.offset, with: testCase.velocity)

            XCTAssertEqual(sheet.state, testCase.state)
        }
    }

    func testDoesNotDismissWhenDraggingDownSlowly() {
        let slowScrollingDownVelocities: [CGPoint] = [
            CGPoint(x: -2, y: -1),
            CGPoint(x: 0, y: -0.5),
            CGPoint(x: 2, y: 0)
        ]

        let testCases = makeTestCases(initialSheetStates: [.expanded],
                                      velocities: slowScrollingDownVelocities)

        testCases.forEach { (testCase) in
            let sheet = TestSheet(initialState: testCase.state)
            sheet.scroll(to: testCase.offset, with: testCase.velocity)

            XCTAssertEqual(sheet.state, testCase.state)
        }
    }

    func testDoesNotDismissWhenDraggingUp() {
        let scrollingUpVelocities: [CGPoint] = [
            CGPoint(x: -2, y: 2),
            CGPoint(x: 0, y: 0.5)
        ]

        let testCases = makeTestCases(initialSheetStates: [.expanded],
                                      velocities: scrollingUpVelocities)

        testCases.forEach { (testCase) in
            let sheet = TestSheet(initialState: testCase.state)
            sheet.scroll(to: testCase.offset, with: testCase.velocity)

            XCTAssertEqual(sheet.state, testCase.state)
        }
    }
}

private struct DismissTestCase {
    let state: SheetState
    let offset: CGPoint
    let velocity: CGPoint

    static let acceptedVelocities: [CGPoint] = [
        CGPoint(x: -2, y: -2),
        CGPoint(x: 0, y: -2),
        CGPoint(x: 2, y: -2)
    ]

    static let acceptedOffsetPoints: [CGPoint] = [
        .zero,
        CGPoint(x: 0, y: -1),
        CGPoint(x: 2, y: 0),
        CGPoint(x: -2, y: 0)
    ]
}

private func makeTestCases(initialSheetStates: [SheetState],
                           offsetPoints: [CGPoint] = DismissTestCase.acceptedOffsetPoints,
                           velocities: [CGPoint] = DismissTestCase.acceptedVelocities ) -> [DismissTestCase]
{
    return initialSheetStates
        .combine(offsetPoints)
        .combine(velocities) { return DismissTestCase(state: $0.0, offset: $0.1, velocity: $1) }
}

private class TestSheet: BottomSheetView {
    var newState: State?
    var isDismissed: Bool = false

    override func change(to state: BottomSheetView.State, animated: Bool = true, completion: @escaping () -> Void) {
        newState = state
        completion()
    }

    func scroll(to offset: CGPoint, with velocity: CGPoint) {
        let pointer = UnsafeMutablePointer<CGPoint>.allocate(capacity: 0)

        scrollView.contentOffset = offset
        scrollViewWillEndDragging(UIScrollView(), withVelocity: velocity, targetContentOffset: pointer)
    }

    // We override this function because the one in BottomSheetView depends on the view frame,
    // which isn't set correctly in the test
    override func offsetPercentage(for state: BottomSheetView.State, offset: CGFloat) -> CGFloat {
        return 0.00
    }
}
