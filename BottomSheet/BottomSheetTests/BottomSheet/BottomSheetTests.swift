//
//  BottomSheetTests.swift
//  BottomSheetTests
//
//  Created by Daneo Van Overloop on 19/02/2019.
//  Copyright © 2019 In The Pocket. All rights reserved.
//

@testable import BottomSheet
import XCTest

class BottomSheetTests: XCTestCase {
    var validationClosure: ((State) -> Void)?
    var dismissalClosure: ((Bool) -> Void)?

    let sheetWithState: (SheetState) -> BottomSheetView = { BottomSheetView(initialState: $0) }

    let sheet = BottomSheetView(initialState: .collapsed)

    override func tearDown() {
        super.tearDown()
        validationClosure = nil
    }

    func testInitialization() {
        let states: [SheetState] = [.expanded, .collapsed]
        states.forEach { state in
            XCTAssertEqual(sheetWithState(state).state, state)
            XCTAssertNil(sheetWithState(state).contentView)
        }
    }

    func testNumberOfSubviews() {
        XCTAssertEqual(sheet.subviews.count, 2) // scroll view, backgroundView
        XCTAssertNil(sheet.contentView)
    }

    func testAddingSubviewConfiguresConstraints() {
        let view = UIView()

        sheet.addSubview(view)
        XCTAssertNotEqual(sheet.constraints, [])
    }

    func testAddingSubviewAddsAsContentView() {
        let numberOfSubviews = sheet.subviews.count
        let view = UIView()

        sheet.addSubview(view)

        XCTAssertEqual(sheet.contentView, view)
        XCTAssertEqual(sheet.subviews.count, numberOfSubviews)
    }

    func testAddingContentViewConfiguresAccessibilityIdentifiers() {
        sheet.addSubview(UIView())

        XCTAssertEqual(sheet.accessibilityIdentifier, BottomSheetView.bottomSheetIdentifier)
        XCTAssertEqual(sheet.contentView?.accessibilityIdentifier, BottomSheetView.sheetContentIdentifier)
    }

    func testAnimationChangesState() {
        let sheet = sheetWithState(.expanded)
        let expectedResult = expectation(description: "Changed state")

        sheet.change(to: .collapsed, completion: {
            XCTAssertEqual(sheet.state, .collapsed)
            expectedResult.fulfill()
        })

        wait(for: [expectedResult], timeout: 0.5)
    }
}
