//
//  ViewController.swift
//  BottomSheet
//
//  Created by Daneo Van Overloop on 13/02/2019.
//  Copyright © 2019 Swiftin. All rights reserved.
//

import Anchorage
import BottomSheet
import UIKit

class ViewController: UIViewController {
    lazy var bottomSheetController = {
        BottomSheetController(initialState: .collapsed)
    }()

    @objc
    func tappedDismiss() {
        bottomSheetController.dismiss(animated: true, completion: nil)
    }

    @IBAction
    func tappedButton() {
        addBottomSheetView()
    }
}

extension ViewController {
    func addBottomSheetView() {
        bottomSheetController.delegate = self
        bottomSheetController.contentView = customView()

        button.addTarget(self, action: #selector(tappedDismiss), for: .touchUpInside)

        present(bottomSheetController, animated: true, completion: nil)
    }
}

extension ViewController: BottomSheetDelegate {
    func changed(to state: State) {
        print("State changed!")
    }

    func dismissed() {
        print("We got dismissed")
    }
}

private let label: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.text = (1 ... 200).map({ "\($0)\n" }).reduce("", +)
    return label
}()

private let view = UIView()

private let button: UIButton = {
    let button = UIButton(type: .system)

    button.setTitle("Fancy pantsy close button", for: .normal)
    button.setTitleColor(.red, for: .normal)
    button.setTitleColor(.yellow, for: .highlighted)
    button.backgroundColor = .orange

    return button
}()

let customView: () -> UIView = {
    view.addSubview(label)
    label.topAnchor == view.topAnchor + 12
    label.horizontalAnchors <= view.horizontalAnchors

    view.addSubview(button)

    button.topAnchor == label.bottomAnchor + 12
    button.heightAnchor == 48
    button.bottomAnchor == view.safeAreaLayoutGuide.bottomAnchor
    button.horizontalAnchors == view.horizontalAnchors

    return view
}
